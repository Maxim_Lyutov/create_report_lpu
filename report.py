import csv
import re

codes = list()

file_diag = open('diag.txt', 'r')
for diag_line in  file_diag:
    codes.append(diag_line.upper().rstrip('\n'))

#print(codes)
file_diag.close

count_diag = {}
count_diag_deti = {}
count_diag_podr = {}

count_podr = 0
count_deti = 0
year_pos = 2019 

file_pos = 'data_all_not_double.txt'
with open(file_pos, 'r', newline='',encoding='utf-8') as flie_data:
    reader = csv.DictReader(flie_data, delimiter='\t')

    for row in reader:
        #year_pos = 2019 #int(row['date_pos'][6:10])
        year_dr = int(row['dr'][6:10])
        
        for code in codes:
            if re.search(code, row['diag']) :
                count_diag[code] = count_diag.get(code, 0) + 1
                
                if  year_pos - year_dr <= 14  :
                    count_diag_deti[code] = count_diag_deti.get(code, 0) + 1
                    #print (row['fio'] ,',', row['dr'], ' :',year_pos ,'-', year_dr)
                    
                elif  year_pos - year_dr > 14  :
                    count_diag_podr[code] = count_diag_podr.get(code, 0) + 1
                    #print (row['fio'] ,',', row['dr'], ' :',year_pos ,'-', year_dr  )
        
        if row['diag'][0:1].find('Z') == -1:
            #print(row['diag'][0:1])
            if  year_pos - year_dr <= 14 :
                count_deti = count_deti + 1
            else:
                count_podr = count_podr + 1

list_sort = list(count_diag.keys())
list_sort.sort()
print('--дети + подростки--------(общий)----------------')
for row in list_sort:
    print(row,':', count_diag[row])

list_sort = list(count_diag_deti.keys())
list_sort.sort()
print('--дети-------------------------------------------')
for row in list_sort:
    print(row,':', count_diag_deti[row])

print('всего детей :', count_deti)

list_sort = list(count_diag_podr.keys())
list_sort.sort()
print('--подростки--------------------------------------')
for row in list_sort:
    print(row,':', count_diag_podr[row])

print('всего подр :', count_podr)

print("читаем файл: ",file_pos) 
flie_data.close